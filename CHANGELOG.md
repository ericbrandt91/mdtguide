Version 1
- Added `/mdtg` chat command to toggle route estimation
- Added experimental route estimation based on shortest path through killed enemies, toggle with `/mdtg route`
- Fixed enemy info frame in guide mode
- Fixed problems after MDT renaming

Version 1-beta3
- Updated TOC version for 8.3

Version 1-beta2
- Added coloring current pull cyan
- Properly handle bosses
- Bugfixes and refinements

Version 1-beta1
- Initial release
- Added compact view for MDT
- Added zooming to selected pull
- Added automatically zooming to current/next pull based on enemy forces
- Added coloring dead enemies red
