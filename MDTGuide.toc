## Interface: 90002
## Title: MDTGuide
## Notes: Guide mode for Mythic Dungeon Tools.
## Notes-deDE: Guide-Modus für Mythic Dungeon Tools.
## Version: @project-version@
## Dependencies: MythicDungeonTools
## SavedVariables: MDTGuideActive, MDTGuideRoute

MDTGuide.lua

#@do-not-package@
## Version: 0-dev0
#@end-do-not-package@
